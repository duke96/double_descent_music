def create_train_command(dataset="jobs_cifar5m", model="cnn", start=1, num_exp=10):
    with open(f"train_{model}.sh", "w") as f:
        f.write(f"#!/bin/bash\n\ncd {dataset}/{model}\n")
        for i in range(start, num_exp+start):
            f.write(f"qsub train{i}.pbs\n")


def create_train_job(i, k, noise_rate, data_size, epoch, folder):
    with open(f"{folder}/train{i}.pbs", "w") as f:
        f.write('#!/bin/bash\n#PBS -P TOP\n#PBS -l walltime=12:00:00\n#PBS -l select=1:ncpus=2:ngpus=1:mem=16GB\n\nmodule load python/3.7.7 magma/2.5.3 openmpi-gcc/3.1.5 cuda/10.2.89\n\ncd "/project/RDS-FEI-DDLN-RW/duke/double_descent_music"\n')
        f.write(f'python train_standard.py --data_path /project/RDS-FEI-DDLN-RW/duke/double_descent_music/data --epochs {epoch} --dataset cifar5m --data_size {data_size} --k {k} --noise_rate {noise_rate} --noise_type symmetric')


ks = [64]
noises = [0.0, 0.5]
# data_sizes = [0.1]
# epoches = [200]
data_sizes = [0.025, 0.5, 0.1, 0.25, 0.5, 1.0]
epoches = [200, 100, 50, 25, 25, 25]
i = 1
folder = "jobs_cifar5m/cnn"

# for k in ks:
#     for noise_rate in noises:
#         for j, data_size in enumerate(data_sizes):
#             epoch = epoches[j]
#             create_train_job(i, k, noise_rate, data_size, epoch, folder)
#             i+=1

create_train_command(dataset="jobs_cifar5m", model="cnn", start=1, num_exp=12)