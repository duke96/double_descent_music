"""Setup."""
import setuptools

# with open("ai_project_template/README.md", "r") as fh:
#     long_description = fh.read()

setuptools.setup(
    name="project",
    version="0.0.1",
    author="Duke",
    author_email="knguyen4@gustavus.edu",
    description="Project for music instruments classification",
    # long_description=long_description,
    # long_description_content_type="text/markdown",
    # install_requires=["tensorflow>=1.8.0", "keras>=2.2.0"],
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(exclude=["ai_project_template_demo"]),
    package_data={"music_noisy": []},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
