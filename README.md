# Double descent on Music dataset

## Initial set up
```
pip install -r requirements.txt
pip install -e .
```


## Organize data
```
python3 preprocess.py
```
Data folder structure original:

    .
    ├── test
    ├── train                    
    │   ├── accordion          
    │   ├── drum         
    │   └── flute                
    └── ...

Data folder structure after run preprocess.py:

    .
    ├── test
    ├── train                    
    │   ├── train000001.png          
    │   ├── train000002.jpg         
    │   └── train000003.jpg    
    ├── label_info.json
    ├── test_filelist.txt
    └── train_filelist.txt

## Train

Train with various layer width (1,2,4,8,16,32,64) and noise_rate (0.1, 0.2, 0.3, ...)

```
#Resnet
python3 train_resnet.py --data_path /project/RDS-FEI-DDLN-RW/duke/double_descent_music/data --epochs 160 --k 8 --dataset cifar10 --noise_rate 0.2

#Standard CNN
python3 train_standard.py --data_path /project/RDS-FEI-DDLN-RW/duke/double_descent_music/data --epochs 160 --k 8 --dataset cifar10 --noise_rate 0.2
```

