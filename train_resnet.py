import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
from data.datasets import CIFAR10, CIFAR5M, FashionMNIST, CIFAR5M10
import torch.nn.functional as F
import torch.optim as optim
import os
import torch.nn as nn
import argparse
import random
from dataloader_music import music_dataloader

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
logger.info(f"Using {device}")

parser = argparse.ArgumentParser(description='Organize raw data')
parser.add_argument('--data_path', default="./data/", type=str, help='path to data folder')
parser.add_argument('--batch_size', default=128, type=int, help='batch size')
parser.add_argument('--lr', type=float, help='learning rate', default=0.001)
parser.add_argument('--num_workers', default=4, type=int, help='')
parser.add_argument('--num_class', default=10, type=int, help='')
parser.add_argument('--epochs', default=150, type=int, help='')
parser.add_argument('--k', default=1, type=int, help='layers width')
parser.add_argument('--resume', default='', type=str, metavar='PATH', help='path to latest checkpoint (default: none)')
parser.add_argument('--start_epoch', default=1, type=int, metavar='PATH', help='where to start training')
parser.add_argument('--dataset', default="cifar10", type=str, metavar='PATH', help='[music, cifar10, cifar10-n, cifar5m, cifar5m10]')
parser.add_argument('--data_size', default=1.0, type=float, metavar='PATH', help='fraction of data to use')
parser.add_argument('--noise_rate', type=float, help='corruption rate, should be less than 1', default=0.2)
parser.add_argument('--noise_type', type=str, help='[pairflip, symmetric]', default='symmetric')

args = parser.parse_args()


## 5-Layer CNN for CIFAR
## Based on https://myrtle.ai/learn/how-to-train-your-resnet-4-architecture/


# class Flatten(nn.Module):
#     def forward(self, x): return x.view(x.size(0), x.size(1))

if not os.path.exists(f"{args.data_path}/logs/resnet"):
    os.mkdir(f"{args.data_path}/logs/resnet")

if args.noise_rate == 0.0:
    args.noise_type = "clean"


class PreActBlock(nn.Module):
    '''Pre-activation version of the BasicBlock.'''
    expansion = 1

    def __init__(self, in_planes, planes, stride=1, **kwargs):
        super(PreActBlock, self).__init__()
        self.bn1 = nn.BatchNorm2d(in_planes)
        self.conv1 = nn.Conv2d(
            in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3,
                               stride=1, padding=1, bias=False)

        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, bias=False)
            )

    def forward(self, x):
        out = F.relu(self.bn1(x))
        shortcut = self.shortcut(out) if hasattr(self, 'shortcut') else x
        out = self.conv1(out)
        out = self.conv2(F.relu(self.bn2(out)))
        out += shortcut
        return out

class PreActResNet(nn.Module):
    def __init__(self, block, num_blocks, num_classes=10, init_channels=64):
        super(PreActResNet, self).__init__()
        self.in_planes = init_channels
        c = init_channels

        self.conv1 = nn.Conv2d(3, c, kernel_size=3,
                               stride=1, padding=1, bias=False)
        self.layer1 = self._make_layer(block, c, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 2*c, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 4*c, num_blocks[2], stride=2)
        self.layer4 = self._make_layer(block, 8*c, num_blocks[3], stride=2)
        self.linear = nn.Linear(8*c*block.expansion, num_classes)

    def _make_layer(self, block, planes, num_blocks, stride):
        # eg: [2, 1, 1, ..., 1]. Only the first one downsamples.
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv1(x)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out


def make_resnet18k(k=64, num_classes=10) -> PreActResNet:
    ''' Returns a ResNet18 with width parameter k. (k=64 is standard ResNet18)'''
    return PreActResNet(PreActBlock, [2, 2, 2, 2], num_classes=num_classes, init_channels=k)


# def adjust_learning_rate(optimizer, t):
#     """
#
#     """
#
#     values = [group['lr'] * self.gamma
#                 for group in optimizer.param_groups]
#     for param_group, lr in zip(optimizer.param_groups, values):
#         param_group['lr'] = lr

# Training
def train(epoch, net, optimizer, criterion, loader):
    logger.info("Start training")
    logger.info('\nEpoch : %d' % epoch)
    loader_size = len(loader)
    net.train()

    running_loss = 0
    correct = 0
    total = 0

    for i, data in enumerate(loader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data[0].to(device), data[1].to(device)

        # zero the parameter gradient
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # calculate statistics
        running_loss += loss.item()

        _, predicted = outputs.max(1)
        total += labels.size(0)
        correct += predicted.eq(labels).sum().item()

        if i % 20 == 0:
            logger.info(f"Progress: {i}/{loader_size}")

    train_loss = running_loss / len(loader)
    accu = 100. * correct / total

    text = 'Epoch %d Train Loss: %.3f | Accuracy: %.3f \n' % (epoch, train_loss, accu)
    with open(f"{args.data_path}/logs/resnet/train_log_{args.dataset}_size={args.data_size}_k={args.k}_noise={args.noise_rate}.txt", "a") as f:
        f.write(text)

    logger.info(text)

    return accu, train_loss


def test(epoch, net, criterion, loader):
    logger.info("Start testing")
    net.eval()

    running_loss = 0
    correct = 0
    total = 0

    with torch.no_grad():
        for i, data in enumerate(loader):
            inputs, labels = data[0].to(device), data[1].to(device)

            # forward
            outputs = net(inputs)
            loss = criterion(outputs, labels)

            # Calculate statistics
            running_loss += loss.item()

            _, predicted = outputs.max(1)
            total += labels.size(0)
            correct += predicted.eq(labels).sum().item()

    test_loss = running_loss / len(loader)
    accu = 100. * correct / total

    text = 'Epoch %d Test Loss: %.3f | Accuracy: %.3f \n' % (epoch, test_loss, accu)
    with open(f"{args.data_path}/logs/resnet/test_log_{args.dataset}_size={args.data_size}_k={args.k}_noise={args.noise_rate}.txt", "a") as f:
        f.write(text)
    logger.info(text)

    return accu, test_loss


def save_checkpoint(state, filename='checkpoint.pth.tar'):
    """
    Save the training model
    """
    torch.save(state, filename)


if __name__ == "__main__":
    # Data loader
    logger.info("Start creating trainloader and testloader")
    if args.dataset == "music":
        loader = music_dataloader(batch_size=args.batch_size, num_class=args.num_class, num_workers=args.num_workers,
                                  root_dir=args.data_path)
        train_loader = loader.run("train")
        test_loader = loader.run("test")

    else:
        if args.dataset == "cifar10":
            train_dataset = CIFAR10(root=args.data_path,
                                    download=True,
                                    train=True,
                                    transform=transforms.ToTensor(),
                                    noise_type=args.noise_type,
                                    noise_rate=args.noise_rate
                                    )

            test_dataset = CIFAR10(root=args.data_path,
                                   download=True,
                                   train=False,
                                   transform=transforms.ToTensor(),
                                   noise_type=args.noise_type,
                                   noise_rate=args.noise_rate
                                   )
        elif args.dataset == "fmnist":
            transform_list = [transforms.Resize(32), transforms.ToTensor(), transforms.Normalize([0.5], [0.5])]
            all_transforms = transforms.Compose(transform_list)
            train_dataset = FashionMNIST(root=args.data_path,
                                         download=True,
                                         train=True,
                                         transform=all_transforms,
                                         noise_type=args.noise_type,
                                         noise_rate=args.noise_rate
                                         )

            test_dataset = FashionMNIST(root=args.data_path,
                                        download=True,
                                        train=False,
                                        transform=all_transforms,
                                        noise_type=args.noise_type,
                                        noise_rate=args.noise_rate
                                        )
        elif args.dataset == "cifar5m":
            train_dataset = CIFAR5M(root=args.data_path,
                                    download=True,
                                    train=True,
                                    transform=transforms.ToTensor(),
                                    noise_type=args.noise_type,
                                    noise_rate=args.noise_rate
                                    )

            test_dataset = CIFAR5M(root=args.data_path,
                                   download=True,
                                   train=False,
                                   transform=transforms.ToTensor(),
                                   noise_type=args.noise_type,
                                   noise_rate=args.noise_rate
                                   )
        elif args.dataset == "cifar5m10":
            train_dataset = CIFAR5M10(root=args.data_path,
                                    download=True,
                                    train=True,
                                    transform=transforms.ToTensor(),
                                    noise_type=args.noise_type,
                                    noise_rate=args.noise_rate
                                    )

            test_dataset = CIFAR5M10(root=args.data_path,
                                   download=True,
                                   train=False,
                                   transform=transforms.ToTensor(),
                                   noise_type=args.noise_type,
                                   noise_rate=args.noise_rate
                                   )

        n = round(args.data_size * len(train_dataset))
        subset = random.sample(list(range(0, len(train_dataset))), n)
        train_dataset = torch.utils.data.Subset(train_dataset, subset)
        logger.info(f"Data size: {len(train_dataset)}")
        train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                                   batch_size=args.batch_size,
                                                   num_workers=args.num_workers,
                                                   drop_last=True,
                                                   shuffle=True)

        test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                                  batch_size=args.batch_size,
                                                  num_workers=args.num_workers,
                                                  drop_last=True,
                                                  shuffle=False)
    logger.info("Done creating trainloader and testloader")

    # Train/eval
    net = make_resnet18k(k=args.k).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9)
    t_max = (args.epochs + 1 - args.start_epoch)  # number of epochs
    scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=t_max, eta_min=0)  # get_last_lr

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            logger.info("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            net.load_state_dict(checkpoint['state_dict'])
            logger.info("=> loaded checkpoint at (epoch {})"
                  .format(checkpoint['epoch']))
        else:
            logger.info("=> no checkpoint found at '{}'".format(args.resume))

    best_acc = 0.0
    for epoch in range(args.start_epoch, args.epochs + 1):
        train(epoch, net, optimizer, criterion, train_loader)
        acc, _ = test(epoch, net, criterion, test_loader)
        scheduler.step()

        is_best = acc > best_acc
        best_acc = max(acc, best_acc)

        if is_best:
            logger.info(f"Saving best checkpoint, test_acc={best_acc}")
            save_checkpoint({
                'epoch': epoch + 1,
                'state_dict': net.state_dict(),
                'best_prec1': best_acc,
            }, filename=os.path.join(args.data_path, f'checkpoint_{args.dataset}_size={args.data_size}_k={args.k}_noise={args.noise_rate}.th'))
    logger.info("Done training")