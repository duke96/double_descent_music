import torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
from data.datasets import CIFAR10, CIFAR5M, FashionMNIST
import torch.optim as optim
import os
import torch.nn as nn
import argparse
import random
from dataloader_music import music_dataloader

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
logger.info(f"Using {device}")

parser = argparse.ArgumentParser(description='Organize raw data')
parser.add_argument('--data_path', default="./data/", type=str, help='path to data folder')
parser.add_argument('--batch_size', default=128, type=int, help='batch size')
parser.add_argument('--lr', type=float, help='learning rate', default=0.001)
parser.add_argument('--num_workers', default=4, type=int, help='')
parser.add_argument('--num_class', default=10, type=int, help='')
parser.add_argument('--num_channel', default=3, type=int, help='')
parser.add_argument('--epochs', default=150, type=int, help='')
parser.add_argument('--k', default=1, type=int, help='layers width')
parser.add_argument('--resume', default='', type=str, metavar='PATH', help='path to latest checkpoint (default: none)')
parser.add_argument('--start_epoch', default=1, type=int, metavar='PATH', help='where to start training')
parser.add_argument('--dataset', default="cifar10", type=str, metavar='PATH', help='[music, cifar10, cifar100, cifar5m, fmnist]')
parser.add_argument('--data_size', default=1.0, type=float, metavar='PATH', help='fraction of data to use')
parser.add_argument('--noise_rate', type=float, help='corruption rate, should be less than 1', default=0.2)
parser.add_argument('--noise_type', type=str, help='[pairflip, symmetric]', default='symmetric')


args = parser.parse_args()


## 5-Layer CNN for CIFAR
## Based on https://myrtle.ai/learn/how-to-train-your-resnet-4-architecture/


# class Flatten(nn.Module):
#     def forward(self, x): return x.view(x.size(0), x.size(1))

if not os.path.exists(f"{args.data_path}/logs/cnn"):
    os.mkdir(f"{args.data_path}/logs/cnn")

if args.noise_rate == 0.0:
    args.noise_type = "clean"


def make_cnn(c=64, num_class=10, num_channel=1, image_size=224):
    ''' Returns a 5-layer CNN with width parameter c. '''
    return nn.Sequential(
        # Layer 0
        nn.Conv2d(num_channel, c, kernel_size=3, stride=1,
                  padding=1, bias=True),
        nn.BatchNorm2d(c),
        nn.ReLU(),      # output: 32 x 256 x 256

        # Layer 1
        nn.Conv2d(c, c*2, kernel_size=3,
                  stride=1, padding=1, bias=True),
        nn.BatchNorm2d(c*2),
        nn.ReLU(),
        nn.MaxPool2d(2),  # output: 64 x 128 x 128

        # Layer 2
        nn.Conv2d(c*2, c*4, kernel_size=3,
                  stride=1, padding=1, bias=True),
        nn.BatchNorm2d(c*4),
        nn.ReLU(),
        nn.MaxPool2d(2),  # output: 128 x 128 x 64

        # Layer 3
        nn.Conv2d(c*4, c*8, kernel_size=3,
                  stride=1, padding=1, bias=True),
        nn.BatchNorm2d(c*8),
        nn.ReLU(),
        nn.MaxPool2d(2),  # output: 256 x 256 x 32

        # Layer 4
        nn.MaxPool2d(4),  # output: 256 x 256 x 8
        nn.Flatten(),
        nn.Linear(c*8*image_size*image_size//32//32, num_class, bias=True)
        # nn.Linear(c * 8, num_classes, bias=True)
    )


# Training
def train(epoch, net, optimizer, criterion, loader):
    logger.info("Start training")
    logger.info('\nEpoch : %d' % epoch)
    loader_size = len(loader)
    net.train()

    running_loss = 0
    correct = 0
    total = 0

    for i, data in enumerate(loader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data[0].to(device), data[1].to(device)

        # zero the parameter gradient
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs.float())
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # calculate statistics
        running_loss += loss.item()

        _, predicted = outputs.max(1)
        total += labels.size(0)
        correct += predicted.eq(labels).sum().item()

        if i % 20 == 0:
            logger.info(f"Progress: {i}/{loader_size}")

    train_loss = running_loss / len(loader)
    accu = 100. * correct / total

    text = 'Epoch %d Train Loss: %.3f | Accuracy: %.3f \n' % (epoch, train_loss, accu)
    with open(f"{args.data_path}/logs/cnn/train_log_{args.dataset}_size={args.data_size}_k={args.k}_noise={args.noise_rate}.txt", "a") as f:
        f.write(text)

    logger.info(text)

    return accu, train_loss


def test(epoch, net, criterion, loader):
    logger.info("Start testing")
    net.eval()

    running_loss = 0
    correct = 0
    total = 0

    with torch.no_grad():
        for i, data in enumerate(loader):
            inputs, labels = data[0].to(device), data[1].to(device)

            # forward
            outputs = net(inputs)
            loss = criterion(outputs, labels)

            # Calculate statistics
            running_loss += loss.item()

            _, predicted = outputs.max(1)
            total += labels.size(0)
            correct += predicted.eq(labels).sum().item()

    test_loss = running_loss / len(loader)
    accu = 100. * correct / total

    text = 'Epoch %d Test Loss: %.3f | Accuracy: %.3f \n' % (epoch, test_loss, accu)
    with open(f"{args.data_path}/logs/cnn/test_log_{args.dataset}_size={args.data_size}_k={args.k}_noise={args.noise_rate}.txt", "a") as f:
        f.write(text)
    logger.info(text)

    return accu, test_loss


def save_checkpoint(state, filename='checkpoint.pth.tar'):
    """
    Save the training model
    """
    torch.save(state, filename)


if __name__ == "__main__":
    # Data loader
    logger.info("Start creating trainloader and testloader")
    if args.dataset == "music":
        loader = music_dataloader(batch_size=args.batch_size, num_class=args.num_class, num_workers=args.num_workers, root_dir=args.data_path)
        train_loader = loader.run("train")
        test_loader = loader.run("test")

    else:
        if args.dataset == "cifar10":
            train_dataset = CIFAR10(root=args.data_path,
                                download=True,
                                train=True,
                                transform=transforms.ToTensor(),
                                noise_type=args.noise_type,
                                noise_rate=args.noise_rate
                                )

            test_dataset = CIFAR10(root=args.data_path,
                               download=True,
                               train=False,
                               transform=transforms.ToTensor(),
                               noise_type=args.noise_type,
                               noise_rate=args.noise_rate
                               )
        elif args.dataset == "fmnist":
            transform_list = [transforms.Resize(32), transforms.ToTensor(), transforms.Normalize([0.5], [0.5])]
            all_transforms = transforms.Compose(transform_list)
            train_dataset = FashionMNIST(root=args.data_path,
                                download=True,
                                train=True,
                                transform=all_transforms,
                                noise_type=args.noise_type,
                                noise_rate=args.noise_rate
                                )

            test_dataset = FashionMNIST(root=args.data_path,
                               download=True,
                               train=False,
                               transform=all_transforms,
                               noise_type=args.noise_type,
                               noise_rate=args.noise_rate
                               )
        elif args.dataset == "cifar5m":
            train_dataset = CIFAR5M(root=args.data_path,
                                download=True,
                                train=True,
                                transform=transforms.ToTensor(),
                                noise_type=args.noise_type,
                                noise_rate=args.noise_rate
                                )

            test_dataset = CIFAR5M(root=args.data_path,
                               download=True,
                               train=False,
                               transform=transforms.ToTensor(),
                               noise_type=args.noise_type,
                               noise_rate=args.noise_rate
                               )

        n = round(args.data_size * len(train_dataset))
        subset = random.sample(list(range(0, len(train_dataset))), n)
        train_dataset = torch.utils.data.Subset(train_dataset, subset)
        logger.info(f"Data size: {len(train_dataset)}")
        train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                                   batch_size=args.batch_size,
                                                   num_workers=args.num_workers,
                                                   drop_last=True,
                                                   shuffle=True)

        test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                                  batch_size=args.batch_size,
                                                  num_workers=args.num_workers,
                                                  drop_last=True,
                                                  shuffle=False)
    logger.info("Done creating trainloader and testloader")

    # Train/eval
    if args.dataset == "music":
        image_size = 224
    else:
        image_size = 32
    net = make_cnn(c=args.k, image_size=image_size, num_class=args.num_class, num_channel=args.num_channel).to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9)
    t_max = (args.epochs + 1 - args.start_epoch)  # number of epochs
    scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=t_max, eta_min=0)  # get_last_lr

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            logger.info("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            net.load_state_dict(checkpoint['state_dict'])
            logger.info("=> loaded checkpoint at (epoch {})"
                  .format(checkpoint['epoch']))
        else:
            logger.info("=> no checkpoint found at '{}'".format(args.resume))

    best_acc = 0.0
    for epoch in range(args.start_epoch, args.epochs + 1):
        train(epoch, net, optimizer, criterion, train_loader)
        acc, _ = test(epoch, net, criterion, test_loader)
        scheduler.step()

        is_best = acc > best_acc
        best_acc = max(acc, best_acc)

        if is_best:
            logger.info(f"Saving best checkpoint, test_acc={best_acc}")
            save_checkpoint({
                'epoch': epoch + 1,
                'state_dict': net.state_dict(),
                'best_prec1': best_acc,
            }, filename=os.path.join(args.data_path, f'checkpoint_{args.dataset}_size={args.data_size}_k={args.k}_noise={args.noise_rate}.th'))
    logger.info("Done training")