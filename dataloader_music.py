from torch.utils.data import Dataset, DataLoader
import torchvision.transforms as transforms
from PIL import Image
import torch
import os
import time


class music_dataset(Dataset):
    def __init__(self, root_dir, transform, mode, num_class):
        self.root = root_dir
        self.transform = transform
        self.mode = mode

        if self.mode == 'test':
            with open(os.path.join(self.root, 'test_filelist.txt')) as f:
                lines = f.readlines()
            self.val_imgs = []
            self.val_labels = {}
            for line in lines:
                img, target = line.split()
                target = int(target)
                if target < num_class:
                    self.val_imgs.append(img)
                    self.val_labels[img] = target
        else:
            with open(os.path.join(self.root, 'train_filelist.txt')) as f:
                lines = f.readlines()
            train_imgs = []
            self.train_labels = {}
            for line in lines:
                img, target = line.split()
                target = int(target)
                if target < num_class:
                    train_imgs.append(img)
                    self.train_labels[img] = target
            if self.mode == 'train':
                self.train_imgs = train_imgs

    def __getitem__(self, index):
        if self.mode == 'train':
            img_path = self.train_imgs[index]
            target = self.train_labels[img_path]
            image = Image.open(os.path.join(self.root, f"train/{img_path}")).convert('RGB')
            img = self.transform(image)
            return img, target
        elif self.mode == 'test':
            img_path = self.val_imgs[index]
            target = self.val_labels[img_path]
            image = Image.open(os.path.join(self.root, f"test/{img_path}")).convert('RGB')
            img = self.transform(image)
            return img, target

    def __len__(self):
        if self.mode!='test':
            return len(self.train_imgs)
        else:
            return len(self.val_imgs)


class music_dataloader():
    def __init__(self, batch_size, num_class, num_workers, root_dir):

        self.batch_size = batch_size
        self.num_class = num_class
        self.num_workers = num_workers
        self.root_dir = root_dir

        self.transform_train = transforms.Compose([
            transforms.Resize(299),
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        ])
        self.transform_test = transforms.Compose([
            transforms.Resize(299),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
        ])

    def run(self, mode):
        if mode == 'train':
            all_dataset = music_dataset(root_dir=self.root_dir, transform=self.transform_train, mode="train",
                                            num_class=self.num_class)
            trainloader = DataLoader(
                dataset=all_dataset,
                batch_size=self.batch_size,
                shuffle=True,
                num_workers=self.num_workers,
                pin_memory=True)
            return trainloader

        elif mode == 'test':
            test_dataset = music_dataset(root_dir=self.root_dir, transform=self.transform_test, mode='test',
                                             num_class=self.num_class)
            test_loader = DataLoader(
                dataset=test_dataset,
                batch_size=self.batch_size,
                shuffle=False,
                num_workers=self.num_workers,
                pin_memory=True)
            return test_loader

        # elif mode == 'eval_train':
        #     eval_dataset = music_dataset(root_dir=self.root_dir, transform=self.transform_test, mode='all',
        #                                      num_class=self.num_class)
        #     eval_loader = DataLoader(
        #         dataset=eval_dataset,
        #         batch_size=self.batch_size * 20,
        #         shuffle=False,
        #         num_workers=self.num_workers,
        #         pin_memory=True)
        #     return eval_loader


if __name__ == "__main__":
    transform_train = transforms.Compose([
        transforms.Resize(299),
        transforms.RandomResizedCrop(224),
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])

    transform_test = transforms.Compose([
        transforms.Resize(299),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])

    data_path = "./train/data"

    train_dataset = music_dataset(root_dir=data_path, transform=transform_train, mode="train",
                                            num_class=10)
    test_dataset = music_dataset(root_dir=data_path, transform=transform_test, mode="test",
                                  num_class=10)

    print(len(train_dataset))
    print(len(test_dataset))

    torch.save(train_dataset, "train_dataset.pt")
    torch.save(test_dataset, "test_dataset.pt")

    time.sleep(2)

    print("---------------")
    train_dataset = torch.load("train_dataset.pt")
    test_dataset = torch.load("test_dataset.pt")
    print(len(train_dataset))
    print(len(test_dataset))


    # print(len(train_dataset))
    # inputs, labels = train_dataset[0]
    # print(inputs.shape)
    # inputs, labels = iter(trainloader)
    # print(inputs.shape)
    # print(labels.shape)


