import shutil
import os
import numpy as np
import json
import argparse

parser = argparse.ArgumentParser(description='Organize raw data')
parser.add_argument('--train_path', default="./data/train", type=str, help='train data')
parser.add_argument('--test_path', default="./data/test", type=str, help='test data')
parser.add_argument('--label_info_path', default="./data/label_info.json", type=str, help='path to store label info')
parser.add_argument('--train_filelist_path', default="./data/train_filelist.txt", type=str, help='path to store label info')
parser.add_argument('--test_filelist_path', default="./data/test_filelist.txt", type=str, help='path to store label info')

args = parser.parse_args()


# train test split
def get_files_from_folder(path):
    files = os.listdir(path)
    return np.asarray(files)


def train_test_split(path_to_data, path_to_test_data, train_ratio):
    # get dirs
    _, dirs, _ = next(os.walk(path_to_data))

    # calculates how many train data per class
    data_counter_per_class = np.zeros((len(dirs)))
    for i in range(len(dirs)):
        path = os.path.join(path_to_data, dirs[i])
        files = get_files_from_folder(path)
        data_counter_per_class[i] = len(files)
    test_counter = np.round(data_counter_per_class * (1 - train_ratio))

    # transfers files
    for i in range(len(dirs)):
        path_to_original = os.path.join(path_to_data, dirs[i])
        path_to_save = os.path.join(path_to_test_data, dirs[i])

        # creates dir
        if not os.path.exists(path_to_save):
            os.makedirs(path_to_save)
        files = get_files_from_folder(path_to_original)
        # moves data
        for j in range(int(test_counter[i])):
            dst = os.path.join(path_to_save, files[j])
            src = os.path.join(path_to_original, files[j])
            shutil.move(src, dst)


def rename_and_organize(mode, label_info, folders):
    """
    Rename and organize data
    :param mode:
    :param label_info:
    :param folders:
    :return:
    """
    index_main = 0
    for folder in folders:
        label = label_info[folder]
        if mode == "train":
            sub_path = args.train_path
            filelist_path = args.train_filelist_path
        else:
            sub_path = args.test_path
            filelist_path = args.test_filelist_path
        folder_path = sub_path + "/" + folder
        try:
            images = os.listdir(folder_path)
        except:
            continue
        valid_ext = [".jpg", ".gif", ".png", ".jpeg"]
        with open(filelist_path, "a") as f:
            for img in images:
                index_main += 1
                index = str(index_main).zfill(6)

                # rename + move file into a central folder
                img_path_old = folder_path + '/' + img
                _, ext = os.path.splitext(img_path_old)
                if ext not in valid_ext:
                    continue
                img = f"{mode}{index}{ext}"
                img_path_new = sub_path + "/" + img
                os.rename(img_path_old, img_path_new)

                # create file list info
                f.write(f"{img} {label}\n")

        # # remove folder
        # shutil.rmtree(folder_path)


def create_label_info():
    """
    Read raw data folder and extract mapping between label info a
    :return:
    """
    data_path = args.test_path
    folders = os.listdir(data_path)
    folders = [f for f in folders if "." not in f]
    label_info = {}
    for i, label in enumerate(folders):
        label_info[label] = i
    with open(args.label_info_path, "w") as f:
        json.dump(label_info, f)
    return label_info, folders


if __name__ == "__main__":
    # Create label info
    label_info, folders = create_label_info()
    # Rename and organize
    rename_and_organize(mode="train", label_info=label_info, folders=folders)
    rename_and_organize(mode="test", label_info=label_info, folders=folders)


